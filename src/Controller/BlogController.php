<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use App\Repository\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(): Response
    {
        
        return $this->render('blog/home.html.twig', [
            'controller_name' => "BlogController",
        ]);
    }

    /**
     * @Route("/blog/slogan/manager", name="blog_slogan_manager")
     */
    public function blogSloganManager(Request $request, EntityManagerInterface $manager, BlogRepository $BlogRepository): Response
    {

        // $blog = new Blog();
        $blog = $BlogRepository->find(1);
        $form = $this->createForm(BlogType::class, $blog);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            
            $manager->persist($blog);
            $manager->flush();

            $this->addFlash("success","Votre slogan a été ajouté");

            return $this->redirectToRoute('home');
        }
        
    
        return $this->render('blog/blog_config.html.twig', [
            'controller_name' => 'BlogController',
            'form' => $form->createView(), 
        ]);
    }


}
